<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $group = Group::where('name', 'admin')->first();
        //dd($group->description);
        return view('home', compact('users','group'));
    }
}
