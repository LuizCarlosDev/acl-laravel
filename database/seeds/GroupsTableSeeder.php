<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            'name' => 'admin',
            'description' => 'usuário com acesso total ao sistema',
        ]);
        DB::table('groups')->insert([
            'name' => 'user',
            'description' => 'usuário com acesso limitado ao sistema',
        ]);
    }
}
