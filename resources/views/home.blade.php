@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h1>Usuários:</h1><a href="{{ url('/register') }}" class="btn-default">Novo</a>
                    <table class="table table-bordered">
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>action</th>
                        </tr>
                        @foreach($users as $user)
                            <tr>
                                <th>{{$user->name}}</th>
                                <th>{{$user->email}}</th>
                                <th>
                                    <a href="#" class="btn btn-primary">Editar</a>
                                @can('delete', $group)
                                    <a href="#" class="btn btn-danger">Deletar</a>

                                @endcan
                                </th>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
